# Seed Dummy Data
A Scala utility to seed dummy data into your datastore

# Near term goals
1. Generate data as a Scala collection and insert to database
1. Start with a single table on a PostgreSQL database
1. Have testing ability using docker-compose with temp containers

# Roadmap
- Seed arbitrary tables in a PostgresSQL db (maybe with a conf)
- Seed arbitrary tables in a BigQuery dataset
- Begin to genericize the RDBS and analytical warehouse approaches
